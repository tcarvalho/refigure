package br.com.refigure.refigure;

/**
 * @author Paresh N. Mayani
 * http://www.technotalkative.com
 */

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class HomeActivity extends DashBoardActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setHeader(getString(R.string.HomeActivityTitle), false);
        
    }
    
    /**
     * Button click handler on Main activity
     * @param v
     */
    public void onButtonClicker(View v)
    {
    	Intent intent;
    	
    	switch (v.getId()) {
            case R.id.main_btn_minions:
                Log.i("HomeActivity", "Minions");
                intent = new Intent(this, Activity_Minions.class);
                startActivity(intent);
                break;
            //case R.id.main_btn_froyo:
            //    intent = new Intent(this, Activity_Froyo.class);
            //    startActivity(intent);
            //    break;
            default:
                break;
		}
    }
}

