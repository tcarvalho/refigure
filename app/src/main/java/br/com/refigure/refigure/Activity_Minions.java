package br.com.refigure.refigure;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

public class Activity_Minions extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        final TouchImageView ivAnim = (TouchImageView)findViewById(R.id.imageView);
        ivAnim.setImageResource(R.drawable.animation_minions);
        ivAnim.setBackgroundColor(Color.rgb(0, 0, 0));
        ivAnim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationDrawable anim = (AnimationDrawable) ivAnim.getDrawable();
                if (anim.isRunning()) {
                    anim.stop();
                } else {
                    anim.start();
                }
            }
        });

        final ImageButton ibtLock = (ImageButton)findViewById(R.id.lockButton);
        ibtLock.setOnClickListener(new View.OnClickListener() {
            AnimationDrawable anim = (AnimationDrawable) ivAnim.getDrawable();

            @Override
            public void onClick(View arg0) {
                if (!ivAnim.isLock()) {
                    ivAnim.lock();
                    ibtLock.setImageResource(R.drawable.lock);
                } else {
                    ivAnim.unlock();
                    ibtLock.setImageResource(R.drawable.unlock);
                }
            }
        });

    }

    private static final int RESULT_SETTINGS = 1;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent i = new Intent(this, PreferencesActivity.class);
                startActivityForResult(i, RESULT_SETTINGS);
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                showPreferences();
                break;

        }

    }

    private void showPreferences() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        StringBuilder builder = new StringBuilder();
        //this.lock = sharedPrefs.getBoolean("prefBlock", false);
    }
}
