package br.com.refigure.refigure;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by tcarvalho on 24/06/2015.
 */
public class PreferencesActivity extends PreferenceActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}